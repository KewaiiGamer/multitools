package com.kewaiigamer.multitools;

import com.kewaiigamer.multitools.proxy.CommonProxy;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;



@Mod(modid = Main.MODID, version = Main.VERSION, name = Main.MODNAME)
public class Main
{
	public static final String MODID = "multitools";
	public static final String MODNAME = "Multi Tools";
	public static final String VERSION = "Alpha-1";
	
	@Instance(MODID)
	public static Main instance = new Main();
	
	@SidedProxy(clientSide = "com.kewaiigamer.multitools.proxy.ClientProxy", serverSide = "com.kewaiigamer.multitools.proxy.ServerProxy")
	public static CommonProxy proxy = new CommonProxy();
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		Main.proxy.preInit(e);
	}
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		Main.proxy.init(e);
	}
	@EventHandler
	public void postInit(FMLPostInitializationEvent e)
	{
		Main.proxy.postInit(e);
	}
	
	
}
