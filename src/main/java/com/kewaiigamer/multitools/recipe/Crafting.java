package com.kewaiigamer.multitools.recipe;

import com.kewaiigamer.multitools.item.ModItems;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Crafting {

	public Crafting() {
		register();
	}
	
	public void register() {
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.woodpaxel, 1), new Object[] {
		"xyz",
		" s ",
		" s ",
		'x', new ItemStack(Items.WOODEN_AXE),
		'y', new ItemStack(Items.WOODEN_PICKAXE),
		'z', new ItemStack(Items.WOODEN_SHOVEL),
		's', new ItemStack(Items.STICK),
		});
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.stonepaxel, 1), new Object[] {
		"xyz",
		" s ",
		" s ",
		'x', new ItemStack(Items.STONE_AXE),
		'y', new ItemStack(Items.STONE_PICKAXE),
		'z', new ItemStack(Items.STONE_SHOVEL),
		's', new ItemStack(Items.STICK),
		});
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.ironpaxel, 1), new Object[] {
		"xyz",
		" s ",
		" s ",
		'x', new ItemStack(Items.IRON_AXE),
		'y', new ItemStack(Items.IRON_PICKAXE),
		'z', new ItemStack(Items.IRON_SHOVEL),
		's', new ItemStack(Items.STICK),
		});
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.goldpaxel, 1), new Object[] {
		"xyz",
		" s ",
		" s ",
		'x', new ItemStack(Items.GOLDEN_AXE),
		'y', new ItemStack(Items.GOLDEN_PICKAXE),
		'z', new ItemStack(Items.GOLDEN_SHOVEL),
		's', new ItemStack(Items.STICK),
		});
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.diamondpaxel, 1), new Object[] {
		"xyz",
		" s ",
		" s ",
		'x', new ItemStack(Items.DIAMOND_AXE),
		'y', new ItemStack(Items.DIAMOND_PICKAXE),
		'z', new ItemStack(Items.DIAMOND_SHOVEL),
		's', new ItemStack(Items.STICK),
		});
		
		}
	}
