package com.kewaiigamer.multitools.item;

import com.kewaiigamer.multitools.CreativeTab;
import com.kewaiigamer.multitools.RegistryUtils;

import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModItems {

	public static Item woodpaxel;
	public static Item stonepaxel;
	public static Item ironpaxel;
	public static Item goldpaxel;
	public static Item diamondpaxel;
	
	public ModItems() {
		createItems();
	}
	public static void createItems() {
		woodpaxel = new WoodPaxel(ToolMaterial.WOOD).setCreativeTab(CreativeTab.tabMultiTools);
		RegistryUtils.setNames(woodpaxel, "woodpaxel");
		GameRegistry.register(woodpaxel);
		stonepaxel = new StonePaxel(ToolMaterial.STONE).setCreativeTab(CreativeTab.tabMultiTools);
		RegistryUtils.setNames(stonepaxel, "stonepaxel");
		GameRegistry.register(stonepaxel);
		ironpaxel = new IronPaxel(ToolMaterial.IRON).setCreativeTab(CreativeTab.tabMultiTools);
		RegistryUtils.setNames(ironpaxel, "ironpaxel");
		GameRegistry.register(ironpaxel);
		goldpaxel = new GoldPaxel(ToolMaterial.GOLD).setCreativeTab(CreativeTab.tabMultiTools);
		RegistryUtils.setNames(goldpaxel, "goldpaxel");
		GameRegistry.register(goldpaxel);
		diamondpaxel = new DiamondPaxel(ToolMaterial.DIAMOND).setCreativeTab(CreativeTab.tabMultiTools);
		RegistryUtils.setNames(diamondpaxel, "diamondpaxel");
		GameRegistry.register(diamondpaxel);
	}
}
